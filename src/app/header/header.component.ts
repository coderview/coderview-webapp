import { Component, OnInit } from '@angular/core';
import {AuthService} from '../service/auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isProfileDropdownShown = false;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  toggleProfileDropdown() {
    this.isProfileDropdownShown = !this.isProfileDropdownShown;
  }

  onLogout() {
    this.authService.logout();
    this.router.navigate(['/']);
  }
}
