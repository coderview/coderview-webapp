import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Challenge} from '../../model/challenge/challenge.model';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {AuthService} from '../../service/auth/auth.service';
import {Router} from '@angular/router';
import {Interview} from '../../model/interview/interview.model';

@Component({
  selector: 'app-challenge-details',
  templateUrl: './challenge-details.component.html',
  styleUrls: ['./challenge-details.component.css']
})
export class ChallengeDetailsComponent implements OnInit {

  @Input() challenge: Challenge;
  @Input() interview: Interview;

  @Output() close = new EventEmitter<Challenge>();
  @Output() include = new EventEmitter<Challenge>();

  editorOptions;

  public Editor = ClassicEditor;

  constructor(private authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
    this.initEditorOptions();
  }

  private initEditorOptions() {
    this.editorOptions = {
      language: this.resolveProgrammingLanguage(),
      readOnly: true
    };
  }

  private resolveProgrammingLanguage(): string {
    const language = this.challenge.programmingLanguage.name;
    if (language === 'Java') {
      return 'java';
    } else if (language === 'Python') {
      return 'python';
    }
  }

  onEdit() {
    this.router.navigate(['/challenges', this.challenge.id, 'edit']);
  }

  onClose() {
    this.close.emit(this.challenge);
  }

  onInclude() {
    this.include.emit(this.challenge);
  }
}
