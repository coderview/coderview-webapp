import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AuthService} from '../service/auth/auth.service';
import {SearchService} from '../service/search/search.service';
import {ChallengeDocumentList} from '../model/search/challenge-document-list.model';
import {SearchCriteria} from '../model/search/search-criteria.model';
import {FormArray, FormControl, FormGroup} from '@angular/forms';
import {ProgrammingLanguageService} from '../service/interview/programming-language.service';
import {ProgrammingLanguage} from '../model/interview/programming-language.model';
import {ChallengeTypeService} from '../service/challenge/challenge-type.service';
import {ChallengeType} from '../model/challenge/challenge-type.model';
import {Challenge} from '../model/challenge/challenge.model';
import {Router} from '@angular/router';
import {ChallengeService} from '../service/challenge/challenge.service';
import {Interview} from '../model/interview/interview.model';

@Component({
  selector: 'app-challenges',
  templateUrl: './challenges.component.html',
  styleUrls: ['./challenges.component.css']
})
export class ChallengesComponent implements OnInit {

  @Input() interview: Interview;

  @Output() includeChallenge = new EventEmitter<Challenge>();

  private searchCriteria: SearchCriteria;

  challenges: ChallengeDocumentList = {numFound: 0, entries: []};

  challengeTypes: ChallengeType[] = [];
  challengeDifficulties = ['Beginner', 'Easy', 'Normal', 'Hard', 'Expert'];
  programmingLanguages: ProgrammingLanguage[] = [];

  form: FormGroup;

  page = 1;
  pageSize = 8;

  SCOPE_GLOBAL = 'global';
  SCOPE_PRIVATE = 'private';
  FAVOURITES = 'favourites';
  scope = this.SCOPE_PRIVATE;

  isChallengeTypesDropdownOpen = false;
  isChallengeDifficultiesDropdownOpen = false;
  isProgrammingLanguagesDropdownOpen = false;

  selectedChallenge: Challenge;

  constructor(private authService: AuthService,
              private searchService: SearchService,
              private programmingLanguageService: ProgrammingLanguageService,
              private challengeTypeService: ChallengeTypeService,
              private challengeService: ChallengeService,
              private router: Router) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      search: new FormControl(''),
      challengeTypesComboBox: new FormArray([]),
      challengeDifficultiesComboBox: new FormArray([]),
      programmingLanguagesComboBox: new FormArray([])
    });

    this.resetSearchCriteria();
    this.initSearch();
    this.initChallengeTypes();
    this.initChallengeDifficulties();
    this.initProgrammingLanguages();
    this.loadChallenges();
  }

  resetSearchCriteria() {
    this.searchCriteria = new SearchCriteria();
    this.searchCriteria.setRows(this.pageSize);
    this.searchCriteria.setSort('lastModifiedDate', 'desc');
  }

  resetForm() {
    this.form.get('search').setValue('');
    (this.form.controls.challengeTypesComboBox as FormArray).controls.forEach(control => control.setValue(false, {emitEvent: false}));
    (this.form.controls.challengeDifficultiesComboBox as FormArray).controls.forEach(control => control.setValue(false, {emitEvent: false}));
    (this.form.controls.programmingLanguagesComboBox as FormArray).controls.forEach(control => control.setValue(false, {emitEvent: false}));
  }

  loadChallenges() {
    this.searchService.getChallenges(this.searchCriteria, this.scope)
      .subscribe((challenges: ChallengeDocumentList) => this.challenges = challenges);
  }

  initSearch() {
    this.form.get('search').valueChanges.subscribe(value => {
      if (value.length !== 1) { // search is performed only for 2 or more characters
        this.searchCriteria.setSearch(value);
        this.loadChallenges();
      }
    });
  }

  initChallengeTypes() {
    this.challengeTypeService.getChallengeTypes()
      .subscribe((challengeTypes: ChallengeType[]) => {
        this.challengeTypes = challengeTypes;

        this.challengeTypes.forEach(challengeType =>
          (this.form.controls.challengeTypesComboBox as FormArray).push(new FormControl(false))
        );

        this.form.get('challengeTypesComboBox').valueChanges.subscribe(values => {
          this.challengeTypes.forEach((challengeType, i) => {
            if (values[i]) {
              this.searchCriteria.addFilter('type', this.challengeTypes[i].name);
            } else {
              this.searchCriteria.removeFilter('type', this.challengeTypes[i].name);
            }
          });
          this.loadChallenges();
        });
      });
  }

  initChallengeDifficulties() {
    this.challengeDifficulties.forEach(difficulty => (this.form.controls.challengeDifficultiesComboBox as FormArray)
      .push(new FormControl(false)));

    this.form.get('challengeDifficultiesComboBox').valueChanges.subscribe(values => {
      this.challengeDifficulties.forEach((challengeDifficulty, i) => {
        if (values[i]) {
          this.searchCriteria.addFilter('difficulty', this.challengeDifficulties[i]);
        } else {
          this.searchCriteria.removeFilter('difficulty', this.challengeDifficulties[i]);
        }
      });

      this.loadChallenges();
    });
  }

  initProgrammingLanguages() {
    this.programmingLanguageService.getProgrammingLanguages()
      .subscribe((programmingLanguages: ProgrammingLanguage[]) => {
        this.programmingLanguages = programmingLanguages;

        this.programmingLanguages.forEach(programmingLanguage =>
          (this.form.controls.programmingLanguagesComboBox as FormArray).push(new FormControl(false))
        );

        this.form.get('programmingLanguagesComboBox').valueChanges.subscribe(values => {
          this.programmingLanguages.forEach((programmingLanguage, i) => {
            if (values[i]) {
              this.searchCriteria.addFilter('programmingLanguage', this.programmingLanguages[i].name);
            } else {
              this.searchCriteria.removeFilter('programmingLanguage', this.programmingLanguages[i].name);
            }
          });

          this.loadChallenges();
        });
      });
  }

  onGlobalScope() {
    if (this.scope !== this.SCOPE_GLOBAL) {
      this.scope = this.SCOPE_GLOBAL;
      this.selectedChallenge = null;
      this.resetSearchCriteria();
      this.resetForm();
      this.loadChallenges();
    }
  }

  onPrivateScope() {
    if (this.scope !== this.SCOPE_PRIVATE) {
      this.scope = this.SCOPE_PRIVATE;
      this.selectedChallenge = null;
      this.resetSearchCriteria();
      this.resetForm();
      this.loadChallenges();
    }
  }

  onNewChallenge() {
    this.router.navigate(['/challenges/new/private']);
  }

  onOpenChallenge(id: string) {
    this.challengeService.getChallenge(+id).subscribe((challenge: Challenge) => {
      this.selectedChallenge = challenge;
    });
  }

  onFavourites() {
    this.scope = this.FAVOURITES;
  }

  onPageChange(page: number) {
    this.searchCriteria.setPage(page);
    this.loadChallenges();
  }

  onClose(challenge) {
    this.selectedChallenge = null;
  }

  onInclude(challenge: Challenge) {
    this.includeChallenge.emit(challenge);
  }
}
