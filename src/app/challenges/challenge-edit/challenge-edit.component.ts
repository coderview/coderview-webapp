import {Component, Input, OnInit} from '@angular/core';
import {Challenge} from '../../model/challenge/challenge.model';
import {ChallengeType} from '../../model/challenge/challenge-type.model';
import {ProgrammingLanguage} from '../../model/interview/programming-language.model';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {ProgrammingLanguageService} from '../../service/interview/programming-language.service';
import {ChallengeTypeService} from '../../service/challenge/challenge-type.service';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {ChallengeService} from '../../service/challenge/challenge.service';

@Component({
  selector: 'app-challenge-edit',
  templateUrl: './challenge-edit.component.html',
  styleUrls: ['./challenge-edit.component.css']
})
export class ChallengeEditComponent implements OnInit {

  challenge: Challenge;
  challengeTypes: ChallengeType[] = [];
  challengeDifficulties = ['Beginner', 'Easy', 'Normal', 'Hard', 'Expert'];
  programmingLanguages: ProgrammingLanguage[] = [];

  editMode = false;
  scope = '';
  editorOptions = {language: 'plaintext'};

  selectedProgrammingLanguage: string;
  selectedChallengeDifficulty: string;
  selectedChallengeType: string;

  public Editor = ClassicEditor;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private programmingLanguageService: ProgrammingLanguageService,
              private challengeTypeService: ChallengeTypeService,
              private challengeService: ChallengeService) {
  }

  ngOnInit() {
    this.initDefaultChallenge();

    this.route.data.subscribe((data: Data) => {
      if (data.challenge) {
        this.challenge = data.challenge;
        this.scope = this.challenge.scope;
        this.editMode = true;

        this.selectedChallengeType = this.challenge.type.name;
        this.selectedChallengeDifficulty = this.challenge.difficulty;
        this.selectedProgrammingLanguage = this.challenge.programmingLanguage.name;

        this.resolveProgrammingLanguage();
      }

      if (data.scope) {
        this.scope = data.scope;
        this.challenge.scope = this.scope.toUpperCase();
      }
    });

    this.challengeTypeService.getChallengeTypes()
      .subscribe((challengeTypes: ChallengeType[]) => {
        this.challengeTypes = challengeTypes;
      });

    this.programmingLanguageService.getProgrammingLanguages()
      .subscribe((programmingLanguages: ProgrammingLanguage[]) => {
        this.programmingLanguages = programmingLanguages;
      });
  }

  private initDefaultChallenge() {
    this.challenge = new Challenge();
    this.challenge.title = '';
    this.challenge.description = '';
    this.challenge.difficulty = '';

    const challengeType = new ChallengeType();
    challengeType.id = -1;

    this.challenge.type = challengeType;
  }

  onSelectChallengeType(name: string) {
    this.challenge.type = this.findChallengeTypeByName(name);
  }

  onSelectChallengeDifficulty(challengeDifficulty) {
    this.challenge.difficulty = challengeDifficulty;
  }

  onSelectProgrammingLanguage(name: string) {
    const programmingLanguage = this.findProgrammingLanguageByName(name);
    this.challenge.programmingLanguage = programmingLanguage;
    this.challenge.sourceCode = programmingLanguage.defaultTemplate;
    this.resolveProgrammingLanguage();
  }

  onSave() {
    if (this.editMode) {
      this.challengeService.updateChallenge(this.challenge.id, this.challenge).subscribe((challenge) => this.navigateToChallenges());
    } else {
      this.challengeService.createChallenge(this.challenge).subscribe((challenge) => this.navigateToChallenges());
    }
  }

  onCancel() {
    this.navigateToChallenges();
  }

  private findProgrammingLanguageByName(name: string) {
    return this.programmingLanguages.find(language => language.name === name);
  }

  private findChallengeTypeByName(name: string) {
    return this.challengeTypes.find(type => type.name === name);
  }

  private navigateToChallenges() {
    this.router.navigate(['/challenges']);
  }

  private resolveProgrammingLanguage() {
    if (this.challenge.programmingLanguage.name === 'Java') {
      this.editorOptions = {language: 'java'};
    } else if (this.challenge.programmingLanguage.name === 'Python') {
      this.editorOptions = {language: 'python'};
    }
  }
}
