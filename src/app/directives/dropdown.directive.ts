import {Directive, ElementRef, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {

  private showClass = 'show';

  constructor(private elementRef: ElementRef, private renderer: Renderer2) {
  }

  @Input('appDropdown') set toggle(isOpen: boolean) {
    if (isOpen) {
      this.renderer.addClass(this.elementRef.nativeElement, this.showClass);
    } else {
      this.renderer.removeClass(this.elementRef.nativeElement, this.showClass);
    }
  }
}
