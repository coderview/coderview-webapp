export class InterviewDocument {
  constructor(public id: string,
              public title: string,
              public status: string,
              public scheduledFor: string,
              public candidateFirstName: string,
              public candidateLastName: string,
              public candidateEmail: string,
              public link: string) {
  }
}
