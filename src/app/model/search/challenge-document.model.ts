export class ChallengeDocument {
  public id: string;
  public title: string;
  public description: string;
  public scope: string;
  public difficulty: string;
  public type: string;
  public owner: string;
  public programmingLanguage: string;

  constructor() {
  }
}
