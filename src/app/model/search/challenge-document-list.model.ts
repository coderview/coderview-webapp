import {ChallengeDocument} from './challenge-document.model';

export class ChallengeDocumentList {
  constructor(public numFound: number,
              public entries: ChallengeDocument[]) {
  }
}
