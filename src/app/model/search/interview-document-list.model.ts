import {InterviewDocument} from './interview-document.model';

export class InterviewDocumentList {
  constructor(public numFound: string,
              public entries: InterviewDocument[]) {
  }
}
