export class SearchCriteria {

  private search: string;
  private filters: { field: string, values: string[] }[];
  private sort: { field: string, order: string };
  private page: number;
  private rows: number;

  constructor() {
    this.search = '';
    this.filters = [];
    this.page = 1;
    this.rows = 10;
  }

  setSearch(search: string) {
    this.search = search;
  }

  addFilter(filterField: string, filterValue: string) {
    const filter = this.findFilter(filterField);

    if (filter) {
      if (!this.filterValueExists(filter, filterValue)) {
        filter.values.push(filterValue);
      }
    } else {
      this.filters.push({field: filterField, values: [filterValue]});
    }
  }

  removeFilter(filterField: string, filterValue: string) {
    const filterIndex = this.findFilterIndex(filterField);

    if (filterIndex >= 0) {
      const filter = this.filters[filterIndex];
      filter.values = filter.values.filter(value => value !== filterValue);

      if (filter.values.length === 0) {
        this.filters.splice(filterIndex, 1);
      }
    }
  }

  setSort(sortField: string, sortOrder: string) {
    this.sort = {field: sortField, order: sortOrder};
  }

  setPage(page: number) {
    this.page = page;
  }

  setRows(rows: number) {
    this.rows = rows;
  }

  private findFilter(field: string) {
    return this.filters.find(filter => filter.field === field);
  }

  private filterValueExists(filter: { field: string, values: string[] }, value: string): boolean {
    return filter.values.some(v => v === value);
  }

  private findFilterIndex(field: string) {
    return this.filters.findIndex(filter => filter.field === field);
  }

  getSearch(): string {
    return this.search;
  }

  getFilters(): { field: string, values: string[] }[] {
    return this.filters;
  }

  getSort(): { field: string, order: string } {
    return this.sort;
  }

  getPage(): number {
    return this.page;
  }

  getRows(): number {
    return this.rows;
  }
}
