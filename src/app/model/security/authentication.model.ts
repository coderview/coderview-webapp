export class AuthenticationResponse {
  constructor(public idToken: string,
              public expiresAt: number) {
  }
}
