export class ExecutionResults {
  public executionId: string;
  public output: string;
  public runtime: number;
  public executedLinesCount: number;
  public programmingLanguage: string;
}
