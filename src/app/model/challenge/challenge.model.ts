import {ChallengeType} from './challenge-type.model';
import {ProgrammingLanguage} from '../interview/programming-language.model';

export class Challenge {
  public id: number;
  public title: string;
  public description: string;
  public sourceCode: string;
  public difficulty: string;
  public type: ChallengeType;
  public programmingLanguage: ProgrammingLanguage;
  public scope: string;

  constructor() {
  }
}
