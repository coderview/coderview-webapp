import {ProgrammingLanguage} from './programming-language.model';

export class Tab {
  public readOnlyId: string;

  constructor(public id: string,
              public title: string,
              public orderNumber: number,
              public programmingLanguage: ProgrammingLanguage,
              public description: string) {
  }
}
