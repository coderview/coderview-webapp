import {InterviewsListItem} from './interviews-list-item.model';

export class InterviewsList {
  constructor(public numFound: number,
              public interviews: InterviewsListItem[]) {
  }
}
