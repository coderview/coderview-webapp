export class Participant {
  constructor(public sessionId: string,
              public name: string,
              public email: string,
              public circleColor: string) {
  }
}
