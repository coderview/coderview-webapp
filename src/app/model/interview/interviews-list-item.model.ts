import {Candidate} from './candidate.model';

export class InterviewsListItem {
  constructor(public id: number,
              public title: string,
              public candidate: Candidate,
              public status: string,
              public scheduledFor: Date,
              public link: string) {
  }
}
