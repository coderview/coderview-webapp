export class Candidate {
  constructor(public firstName: string,
              public lastName: string,
              public email: string) {
  }
}
