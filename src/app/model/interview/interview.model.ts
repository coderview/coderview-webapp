import {Candidate} from './candidate.model';
import {InterviewEnvironment} from './interview-environment.model';

export class Interview {
  public startedAt: Date;
  public completedAt: Date;

  constructor(public id: number,
              public title: string,
              public link: string,
              public interviewer: string,
              public status: string,
              public scheduledFor: Date,
              public candidate: Candidate,
              public environment?: InterviewEnvironment) {
  }
}
