import {Tab} from './tab.model';

export class InterviewEnvironment {
  constructor(public id: number,
              public notes: string,
              public tabs: Tab[]) {
  }
}
