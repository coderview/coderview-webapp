export class ProgrammingLanguage {

  constructor(public name: string,
              public version: string,
              public info: string,
              public defaultTemplate: string) {
  }
}
