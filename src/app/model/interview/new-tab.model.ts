export class NewTab {
  constructor(public name: string,
              public programmingLanguage: string) {
  }
}
