import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-interview-end',
  templateUrl: './interview-end.component.html',
  styleUrls: ['./interview-end.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class InterviewEndComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
