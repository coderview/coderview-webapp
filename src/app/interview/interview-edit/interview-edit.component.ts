import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {Interview} from '../../model/interview/interview.model';
import * as moment from 'moment';
import {Candidate} from '../../model/interview/candidate.model';
import {InterviewService} from '../../service/interview/interview.service';

@Component({
  selector: 'app-interview-edit',
  templateUrl: './interview-edit.component.html',
  styleUrls: ['./interview-edit.component.css']
})
export class InterviewEditComponent implements OnInit {

  @ViewChild('interviewForm', {static: false}) interviewForm: NgForm;

  interview: Interview;

  editMode = false;

  date: { year: number, month: number, day: number };
  time: { hour: number, minute: number };

  constructor(private route: ActivatedRoute, private router: Router, private interviewService: InterviewService) {
  }

  ngOnInit() {
    this.route.data.subscribe((data: Data) => {
      this.interview = data.interview;
      this.initForm();
    });
  }

  private initForm() {
    if (this.interview) {
      this.editMode = true;
      const scheduledFor = moment(this.interview.scheduledFor.toISOString());

      this.date = {
        year: scheduledFor.year(),
        month: scheduledFor.month() + 1, // Adding 1 because Moment.js use 0-based months however months is ng-bootstrap are 1-based
        day: scheduledFor.date()
      };

      this.time = {
        hour: scheduledFor.hour(),
        minute: scheduledFor.minute()
      };
    } else {
      this.interview = new Interview(null, null, null, null, null , null,
        new Candidate(null, null, null));
    }
  }

  isDateValid(): boolean {
    return true;
  }

  onSave() {
    this.interview.scheduledFor = moment({
      year: this.date.year,
      month: this.date.month - 1,
      day: this.date.day,
      hour: this.time.hour,
      minute: this.time.minute
    }).toDate();

    if (this.editMode) {
      this.interviewService.updateInterview(this.interview.id, this.interview).subscribe(() => this.navigateToInterviewList());
    } else {
      this.interviewService.createInterview(this.interview).subscribe(() => this.navigateToInterviewList());
    }
  }

  navigateToInterviewList() {
    this.router.navigate(['/interviews']);
  }

  onCancel() {
    this.navigateToInterviewList();
  }
}
