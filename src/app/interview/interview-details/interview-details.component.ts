import {Component, OnInit} from '@angular/core';
import {Interview} from '../../model/interview/interview.model';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {InterviewMode} from '../../service/interview/interview-mode.enum';
import {InterviewService} from '../../service/interview/interview.service';
import {environment} from '../../../environments/environment';
import * as moment from 'moment';

@Component({
  selector: 'app-interview-details',
  templateUrl: './interview-details.component.html',
  styleUrls: ['./interview-details.component.css']
})
export class InterviewDetailsComponent implements OnInit {

  interview: Interview;
  mode: InterviewMode = InterviewMode.PREVIEW;

  duration: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private interviewService: InterviewService) {
  }

  ngOnInit() {
    this.route.data.subscribe((data: Data) => {
      this.interview = data.interview;

      const durationInSeconds = moment(this.interview.completedAt).diff(moment(this.interview.startedAt), 'seconds');
      if (durationInSeconds > 0) {
        const hours = Math.floor(durationInSeconds / 3600);
        const minutes = Math.floor((durationInSeconds - hours * 3600) / 60);
        const seconds = durationInSeconds - hours * 3600 - minutes * 60;

        const hoursExpression = hours >= 10 ? hours + '' : '0' + hours;
        const minutesExpression = minutes >= 10 ? minutes + '' : '0' + minutes;
        const secondsExpression = seconds >= 10 ? seconds + '' : '0' + seconds;

        this.duration = hoursExpression + ':' + minutesExpression + ':' + secondsExpression;
      }
    });
  }

  onSetupEnvironment() {
    this.router.navigate(['/interviews', this.interview.id, 'setup']);
  }

  onStart() {
    this.interviewService.start(this.interview.id).subscribe(() => {
      this.navigateToInterview();
    });
  }

  onJoin() {
    this.navigateToInterview();
  }

  isInterviewScheduled(): boolean {
    return this.interview.status === 'SCHEDULED';
  }

  isInterviewInProgress(): boolean {
    return this.interview.status === 'IN_PROGRESS';
  }

  isInterviewCompleted(): boolean {
    return this.interview.status === 'COMPLETED';
  }

  private navigateToInterview() {
    this.router.navigate(['/i', this.interview.link]);
  }

  getFullLink() {
    return environment.webappBaseUri + '/i/' + this.interview.link;
  }
}
