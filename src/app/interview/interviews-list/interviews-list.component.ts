import {Component, ElementRef, OnInit, Renderer2} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SearchService} from '../../service/search/search.service';
import {SearchCriteria} from '../../model/search/search-criteria.model';
import {FormControl, FormGroup} from '@angular/forms';
import {InterviewsList} from '../../model/interview/interviews-list.model';
import {InterviewService} from '../../service/interview/interview.service';
import {environment} from '../../../environments/environment';
import {InterviewsListItem} from '../../model/interview/interviews-list-item.model';

@Component({
  selector: 'app-interviews-list',
  templateUrl: './interviews-list.component.html',
  styleUrls: ['./interviews-list.component.css']
})
export class InterviewsListComponent implements OnInit {

  private searchCriteria: SearchCriteria;

  interviewsList = new InterviewsList(0, []);
  isStatusDropdownOpen: boolean;

  form: FormGroup;

  isSortOrderAsc = false;

  page = 1;
  pageSize = 8;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private searchService: SearchService,
              private interviewService: InterviewService,
              private renderer: Renderer2) {
  }

  ngOnInit() {
    this.isStatusDropdownOpen = false;
    this.searchCriteria = new SearchCriteria();
    this.searchCriteria.setRows(this.pageSize);
    this.searchCriteria.setSort('lastEditedAt', 'desc');
    this.form = new FormGroup({
      search: new FormControl(''),
      statusScheduled: new FormControl(false),
      statusInProgress: new FormControl(false),
      statusCompleted: new FormControl(false)
    });
    this.loadInterviews();

    this.form.get('search').valueChanges.subscribe(value => {
      if (value.length !== 1) { // search does not work for single character
        this.searchCriteria.setSearch(value);
        this.loadInterviews();
      }
    });

    this.form.get('statusScheduled').valueChanges.subscribe(value => {
      this.filterByStatusEvent('SCHEDULED', value);
    });

    this.form.get('statusInProgress').valueChanges.subscribe(value => {
      this.filterByStatusEvent('IN_PROGRESS', value);
    });

    this.form.get('statusCompleted').valueChanges.subscribe(value => {
      this.filterByStatusEvent('COMPLETED', value);
    });
  }

  private filterByStatusEvent(status: string, checked: boolean) {
    if (checked) {
      this.searchCriteria.addFilter('status', status);
    } else {
      this.searchCriteria.removeFilter('status', status);
    }

    this.loadInterviews();
  }

  private loadInterviews() {
    this.searchService.getInterviews(this.searchCriteria)
      .subscribe((interviews: InterviewsList) => this.interviewsList = interviews);
  }

  onSort(field: string) {
    this.searchCriteria.setSort(field, this.isSortOrderAsc ? 'asc' : 'desc');
    this.isSortOrderAsc = !this.isSortOrderAsc;
    this.loadInterviews();
  }

  onScheduleNew() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  onEdit(id: number) {
    this.router.navigate([id, 'edit'], {relativeTo: this.route});
  }

  onDelete(id: number) {
    this.interviewService.deleteInterview(id).subscribe(() => this.loadInterviews());
  }

  onPageChange(page: number) {
    this.searchCriteria.setPage(page);
    this.loadInterviews();
  }

  onCopyLink(interview: InterviewsListItem) {
    console.log(this.getFullLink(interview));
  }

  getFullLink(interview: InterviewsListItem) {
    return environment.webappBaseUri + '/i/' + interview.link;
  }
}
