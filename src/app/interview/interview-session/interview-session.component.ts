import {Component, OnDestroy, OnInit} from '@angular/core';
import {Interview} from '../../model/interview/interview.model';
import {InterviewMode} from '../../service/interview/interview-mode.enum';
import {ActivatedRoute, Data} from '@angular/router';
import {SocketClientService} from '../../service/websocket/socket-client.service';
import {AuthService} from '../../service/auth/auth.service';

@Component({
  selector: 'app-interview-session',
  templateUrl: './interview-session.component.html',
  styleUrls: ['./interview-session.component.css']
})
export class InterviewSessionComponent implements OnInit, OnDestroy {

  interview: Interview;
  mode: InterviewMode = InterviewMode.ACTIVE;

  constructor(private route: ActivatedRoute,
              private socketClientService: SocketClientService,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.route.data.subscribe((data: Data) => {
      this.interview = data.interview;
      this.socketClientService.connect(this.getHeaders());
    });
  }

  ngOnDestroy() {
    this.socketClientService.disconnect();
  }

  private getHeaders() {
    if (this.authService.isLoggedIn()) {
      return {
        link: this.interview.link,
        user: this.authService.getLoggedInUser()
      };
    } else {
      return {
        link: this.interview.link
      };
    }
  }
}
