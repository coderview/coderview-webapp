import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {InterviewMode} from '../../../service/interview/interview-mode.enum';
import * as moment from 'moment';
import {Subscription, timer} from 'rxjs';
import {InterviewService} from '../../../service/interview/interview.service';
import {Participant} from '../../../model/interview/participant.model';
import {SocketClientService} from '../../../service/websocket/socket-client.service';

@Component({
  selector: 'app-interview-management-toolbar',
  templateUrl: './interview-management-toolbar.component.html',
  styleUrls: ['./interview-management-toolbar.component.css']
})
export class InterviewManagementToolbarComponent implements OnInit, OnDestroy {

  @Input() mode: InterviewMode;
  @Input() isInterviewer: boolean;
  @Input() startedAt: Date;
  @Input() interviewId: number;
  @Input() interviewLink: string;

  @Output() openChallenges = new EventEmitter<boolean>();
  @Output() endInterview = new EventEmitter<boolean>();

  duration: string;

  durationSubscription: Subscription;

  participants: Participant[] = [];

  constructor(private interviewService: InterviewService,
              private socketClientService: SocketClientService) {
  }

  ngOnInit() {
    if (this.isActive()) {
      this.socketClientService.connected.subscribe(v => {
        this.interviewService.getParticipants(this.interviewId).subscribe(response => this.participants = response);
      });

      this.socketClientService.onMessage('/topic/' + this.interviewLink + '/participants/joined', SocketClientService.jsonHandler)
        .subscribe((participant) => this.addParticipant(participant));

      this.socketClientService.onMessage('/topic/' + this.interviewLink + '/participants/left', SocketClientService.jsonHandler)
        .subscribe((participant) => this.removeParticipant(participant));

      this.durationSubscription = timer(0, 1000).subscribe(v => {
        const durationInSeconds = moment(new Date()).diff(moment(this.startedAt), 'seconds');
        if (durationInSeconds > 0) {
          const hours = Math.floor(durationInSeconds / 3600);
          const minutes = Math.floor((durationInSeconds - hours * 3600) / 60);
          const seconds = durationInSeconds - hours * 3600 - minutes * 60;

          const hoursExpression = hours >= 10 ? hours + '' : '0' + hours;
          const minutesExpression = minutes >= 10 ? minutes + '' : '0' + minutes;
          const secondsExpression = seconds >= 10 ? seconds + '' : '0' + seconds;

          this.duration = hoursExpression + ':' + minutesExpression + ':' + secondsExpression;
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.isActive()) {
      this.durationSubscription.unsubscribe();
    }
  }

  private addParticipant(participant: Participant) {
    if (!this.participantExists(participant)) {
      this.participants.push(participant);
    }
  }

  private removeParticipant(participant: Participant) {
    if (this.participantExists(participant)) {
      const index = this.findParticipantIndex(participant);
      this.participants.splice(index, 1);
    }
  }

  private participantExists(p: Participant): boolean {
    if (this.participants.length === 0) {
      return false;
    }
    return this.participants.some(participant => participant.email === p.email && participant.name === p.name);
  }

  private findParticipantIndex(p: Participant): number {
    return this.participants.findIndex(participant => participant.email === p.email && participant.name === p.name);
  }

  onCall() {
  }

  isActive(): boolean {
    return this.mode === InterviewMode.ACTIVE;
  }

  isNotActive(): boolean {
    return this.mode !== InterviewMode.ACTIVE;
  }

  isInPreviewMode(): boolean {
    return this.mode === InterviewMode.PREVIEW;
  }

  onOpenChallenges() {
    this.openChallenges.emit(true);
  }

  onEndInterview() {
    this.endInterview.emit(true);
  }
}
