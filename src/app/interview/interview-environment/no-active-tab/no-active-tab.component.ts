import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-no-active-tab',
  templateUrl: './no-active-tab.component.html',
  styleUrls: ['./no-active-tab.component.css']
})
export class NoActiveTabComponent implements OnInit {

  @Input() isInSetupMode: boolean;

  constructor() { }

  ngOnInit() {
  }

}
