import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {InterviewEnvironmentService} from '../../../service/interview/interview-environment.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit, OnDestroy {

  @Input() isDisabled: boolean;
  @Input() environmentId: number;

  notes = '';

  constructor(private interviewEnvironmentService: InterviewEnvironmentService) {
  }

  ngOnInit() {
    // Hack to load notes
    setTimeout(() => this.interviewEnvironmentService.getNotes(this.environmentId)
        .subscribe(notes => {
          this.notes = notes;
        }), 1000);
  }

  ngOnDestroy(): void {
    if (!this.isDisabled) {
      this.interviewEnvironmentService.saveNotes(this.environmentId, this.notes).subscribe(() => {
        console.log('Notes saved.');
      });
    }
  }
}
