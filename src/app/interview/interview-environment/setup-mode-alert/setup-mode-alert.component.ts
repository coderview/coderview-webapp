import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-setup-mode-alert',
  templateUrl: './setup-mode-alert.component.html',
  styleUrls: ['./setup-mode-alert.component.css']
})
export class SetupModeAlertComponent implements OnInit {

  @Input() interviewId: number;
  @Input() splitSize: number;

  constructor() {
  }

  ngOnInit() {
  }

}
