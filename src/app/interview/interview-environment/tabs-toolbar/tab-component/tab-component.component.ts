import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Tab} from '../../../../model/interview/tab.model';

@Component({
  selector: 'app-tab-component',
  templateUrl: './tab-component.component.html',
  styleUrls: ['./tab-component.component.css']
})
export class TabComponentComponent implements OnInit {

  @Output() openTab = new EventEmitter<Tab>();
  @Output() closeTab = new EventEmitter<Tab>();

  @Input() tab: Tab;
  @Input() isActive: boolean;
  @Input() isCloseDisabled: boolean;

  constructor() { }

  ngOnInit() {
  }

  onOpenTab() {
    this.openTab.emit(this.tab);
  }

  onCloseTab(event: MouseEvent) {
    this.isCloseDisabled = true;
    event.stopPropagation();
    this.closeTab.emit(this.tab);
  }
}
