import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ProgrammingLanguage} from '../../../../model/interview/programming-language.model';
import {NgForm} from '@angular/forms';
import {NewTab} from '../../../../model/interview/new-tab.model';
import {ProgrammingLanguageService} from '../../../../service/interview/programming-language.service';

@Component({
  selector: 'app-new-tab-dropdown',
  templateUrl: './new-tab-dropdown.component.html',
  styleUrls: ['./new-tab-dropdown.component.css']
})
export class NewTabDropdownComponent implements OnInit, AfterViewInit {

  @ViewChild('newTabForm', {static: false}) newTabForm: NgForm;

  @Output() newTab = new EventEmitter<NewTab>();

  @Input() isDisabled: boolean;

  programmingLanguages: ProgrammingLanguage[];
  isOpen = false;

  constructor(private programmingLanguageService: ProgrammingLanguageService) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.programmingLanguageService.getProgrammingLanguages()
      .subscribe((programmingLanguages: ProgrammingLanguage[]) => {
        this.programmingLanguages = programmingLanguages;
        this.resetNewTabForm();
      });
  }

  closeDropdown() {
    this.isOpen = false;
  }

  toggleDropdown() {
    this.isOpen = !this.isOpen;
  }

  resetNewTabForm() {
    this.newTabForm.form.patchValue({
      name: '',
      programmingLanguage: this.getFirstProgrammingLanguage()
    });
  }

  onAddTab() {
    this.newTab.emit(new NewTab(this.newTabForm.value.name, this.newTabForm.value.programmingLanguage));
    this.isOpen = false;
    this.resetNewTabForm();
  }

  private getFirstProgrammingLanguage(): string {
    if (this.programmingLanguages && this.programmingLanguages.length > 0) {
      return this.programmingLanguages[0].name;
    } else {
      return '';
    }
  }
}
