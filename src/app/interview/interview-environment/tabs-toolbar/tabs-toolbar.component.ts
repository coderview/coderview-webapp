import {Component, Input, OnInit} from '@angular/core';
import {Tab} from '../../../model/interview/tab.model';
import {NewTab} from '../../../model/interview/new-tab.model';
import {TabService} from '../../../service/interview/tab.service';
import {ExecutionService} from '../../../service/execution/execution.service';
import {ExecutionResults} from '../../../model/execution/execution-results.model';
import {InterviewMode} from '../../../service/interview/interview-mode.enum';
import {SocketClientService} from '../../../service/websocket/socket-client.service';

@Component({
  selector: 'app-tabs-toolbar',
  templateUrl: './tabs-toolbar.component.html',
  styleUrls: ['./tabs-toolbar.component.css']
})
export class TabsToolbarComponent implements OnInit {

  @Input() tabs: Tab[];
  @Input() environmentId: number;
  @Input() interviewLink: string;
  @Input() mode: InterviewMode;

  activeTab: Tab;
  isRunning = false;

  constructor(private tabService: TabService,
              private executionService: ExecutionService,
              private socketClientService: SocketClientService) {
  }

  ngOnInit() {
    if (this.isActive()) {
      this.socketClientService.onMessage('/topic/' + this.interviewLink + '/tabs/new', SocketClientService.jsonHandler)
        .subscribe((tab) => this.addTab(tab));

      this.socketClientService.onMessage('/topic/' + this.interviewLink + '/tabs/delete', SocketClientService.textHandler)
        .subscribe((id) => this.closeTab(id));
    }

    this.tabService.includeChallenge.subscribe((tab: Tab) => this.addTab(tab));

    this.openFirstTab();
  }

  openFirstTab() {
    if (this.tabs && this.tabs.length > 0) {
      this.openTab(this.tabs[0]);
    }
  }

  openTab(tab: Tab) {
    this.activeTab = tab;
    this.tabService.tabOpen.next(tab);
  }

  createTab(newTab: NewTab) {
    this.tabService.createTab(this.environmentId, newTab.name, newTab.programmingLanguage)
      .subscribe((tab: Tab) => this.addTab(tab));
  }

  private addTab(tab: Tab) {
    if (!this.tabExists(tab.id)) {
      this.openTab(tab);
      this.tabs.push(tab);
    }
  }

  private tabExists(id: string): boolean {
    if (!this.tabs || this.tabs.length === 0) {
      return false;
    }
    return this.tabs.some(tab => tab.id === id);
  }

  private findTabIndex(id: string): number {
    return this.tabs.findIndex(tab => tab.id === id);
  }

  deleteTab(tab: Tab) {
    this.tabService.deleteTab(this.environmentId, tab.id).subscribe(() => {
      this.closeTab(tab.id);
    });
  }

  closeTab(id: string) {
    if (this.tabExists(id)) {
      const index = this.findTabIndex(id);
      this.tabs.splice(index, 1);
      if (this.activeTab.id === id) {
        this.resolveNextActiveTab(index);
      }
    }
  }

  private resolveNextActiveTab(index: number) {
    if (this.tabs.length === 0) {
      this.openTab(null);
    } else {
      if (index === 0) {
        this.openTab(this.tabs[0]);
      } else {
        this.openTab(this.tabs[index - 1]);
      }
    }
  }

  onRun() {
    this.isRunning = true;
    this.executionService.executeTab(this.activeTab.id).subscribe(
      (executionResults: ExecutionResults) => {
        this.isRunning = false;
        this.executionService.newExecutionResult.next(executionResults);
      },
      () => this.isRunning = false,
      () => this.isRunning = false
    );
  }

  isInPreviewMode(): boolean {
    return this.mode === InterviewMode.PREVIEW;
  }

  isActive(): boolean {
    return this.mode === InterviewMode.ACTIVE;
  }
}
