import {Component, Input, OnInit} from '@angular/core';
import {ExecutionResults} from '../../../model/execution/execution-results.model';
import {ExecutionService} from '../../../service/execution/execution.service';
import {SocketClientService} from '../../../service/websocket/socket-client.service';
import {InterviewMode} from '../../../service/interview/interview-mode.enum';

@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.css']
})
export class OutputComponent implements OnInit {

  @Input() isDisabled;
  @Input() mode: InterviewMode;
  @Input() interviewLink: string;

  executionLog: ExecutionResults[] = [];

  constructor(private executionService: ExecutionService,
              private socketClientService: SocketClientService) {
  }

  ngOnInit() {
    if (this.isActive()) {
      this.socketClientService.onMessage('/topic/' + this.interviewLink + '/execution/output', SocketClientService.jsonHandler)
        .subscribe((executionResult) => this.addExecutionResult(executionResult));
    }

    this.executionService.newExecutionResult.subscribe(executionResult => this.addExecutionResult(executionResult));
  }

  onClearOutput() {
    this.executionLog = [];
  }

  private addExecutionResult(executionResult: ExecutionResults) {
    if (!this.executionExists(executionResult)) {
      this.executionLog.push(executionResult);
    }
  }

  private executionExists(executionResult: ExecutionResults) {
    if (this.executionLog.length === 0) {
      return false;
    }
    return this.executionLog.some(log => log.executionId === executionResult.executionId);
  }

  isActive(): boolean {
    return this.mode === InterviewMode.ACTIVE;
  }
}
