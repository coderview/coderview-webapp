import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {InterviewMode} from '../../service/interview/interview-mode.enum';
import {SocketClientService} from '../../service/websocket/socket-client.service';
import {Interview} from '../../model/interview/interview.model';
import {ProgrammingLanguageService} from '../../service/interview/programming-language.service';
import {Tab} from '../../model/interview/tab.model';
import {environment} from '../../../environments/environment';
import {TabService} from '../../service/interview/tab.service';
import {InterviewService} from '../../service/interview/interview.service';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {Challenge} from '../../model/challenge/challenge.model';
import {InterviewEnvironmentService} from '../../service/interview/interview-environment.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-interview-environment',
  templateUrl: './interview-environment.component.html',
  styleUrls: ['./interview-environment.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class InterviewEnvironmentComponent implements OnInit {

  @Input() mode: InterviewMode;
  @Input() interview: Interview;

  activeTab: Tab;
  padSrc: string;
  padOptions = '?showLineNumbers=true&showControls=false&showChat=false&alwaysShowChat=false&noColors=true';
  showIframeHider = false;
  isInterviewer: boolean;

  modalOptions: NgbModalOptions = {
    size: 'xl'
  };

  constructor(private route: ActivatedRoute,
              private router: Router,
              private socketClientService: SocketClientService,
              private programmingLanguageService: ProgrammingLanguageService,
              private tabService: TabService,
              private interviewService: InterviewService,
              private modalService: NgbModal,
              private interviewEnvironmentService: InterviewEnvironmentService) {
  }

  ngOnInit() {
    this.isInterviewer = this.interviewService.isInterviewer(this.interview);

    if (!this.isInterviewer && this.isActive()) {
      this.socketClientService.onMessage('/topic/' + this.interview.link + '/interview/end', SocketClientService.textHandler)
        .subscribe(() => {
          this.router.navigate(['end'], {relativeTo: this.route});
        });
    }

    this.tabService.tabOpen.subscribe(
      (tab: Tab) => {
        if (this.mode === InterviewMode.PREVIEW) {
          this.padSrc = tab ? environment.etherpadUrl + '/ro/' + tab.readOnlyId + this.padOptions : null;
        } else {
          this.padSrc = tab ? environment.etherpadUrl + '/p/' + tab.id + this.padOptions : null;
        }
        this.activeTab = tab;
      });
  }

  isActive(): boolean {
    return this.mode === InterviewMode.ACTIVE;
  }

  isInSetupMode(): boolean {
    return this.mode === InterviewMode.SETUP;
  }

  isInPreviewMode(): boolean {
    return this.mode === InterviewMode.PREVIEW;
  }

  onOpenChallenges(challengesModal) {
    this.modalService.open(challengesModal, this.modalOptions);
  }

  onEndInterview(endInterviewConfirmationModal) {
    this.modalService.open(endInterviewConfirmationModal, {
      size: 'sm',
      centered: true
    });
  }

  onCloseEndInterviewModal() {
    this.modalService.dismissAll('');
  }

  onConfirmEndInterview() {
    this.interviewService.end(this.interview.id).subscribe(() => {
      this.modalService.dismissAll('');
      this.router.navigate(['/interviews', this.interview.id]);
    });
  }

  onIncludeChallenge(challenge: Challenge) {
    this.interviewEnvironmentService.includeChallenge(this.interview.environment.id, challenge).subscribe((tab: Tab) => {
      this.tabService.includeChallenge.next(tab);
      this.modalService.dismissAll('Challenge Included');
    });
  }
}
