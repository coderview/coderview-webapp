import { Component, OnInit } from '@angular/core';
import {Interview} from '../../model/interview/interview.model';
import {InterviewMode} from '../../service/interview/interview-mode.enum';
import {ActivatedRoute, Data} from '@angular/router';

@Component({
  selector: 'app-interview-setup',
  templateUrl: './interview-setup.component.html',
  styleUrls: ['./interview-setup.component.css']
})
export class InterviewSetupComponent implements OnInit {

  interview: Interview;
  mode: InterviewMode = InterviewMode.SETUP;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe((data: Data) => {
      this.interview = data.interview;
    });
  }

}
