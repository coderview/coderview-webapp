import {SearchCriteria} from '../model/search/search-criteria.model';
import {HttpParams} from '@angular/common/http';

export class SearchParamsBuilder {

  private searchKey = '_search';
  private filterKey = '_filter';
  private sortKey = '_sort';
  private pageKey = '_page';
  private rowsKey = '_rows';

  build(searchCriteria: SearchCriteria): HttpParams {
    let searchParams = new HttpParams();

    if (searchCriteria.getSearch() && searchCriteria.getSearch().length > 0) {
      searchParams = searchParams.set(this.searchKey, searchCriteria.getSearch());
    }

    if (searchCriteria.getFilters() && searchCriteria.getFilters().length > 0) {
      const filterExpression = searchCriteria.getFilters()
        .map(filter => filter.field + ':' + filter.values.join('|'))
        .join(',');
      searchParams = searchParams.set(this.filterKey, filterExpression);
    }

    if (searchCriteria.getSort()) {
      searchParams = searchParams.set(this.sortKey, searchCriteria.getSort().field + ':' + searchCriteria.getSort().order);
    }

    searchParams = searchParams.set(this.pageKey, '' + searchCriteria.getPage());

    searchParams = searchParams.set(this.rowsKey, '' + searchCriteria.getRows());

    return searchParams;
  }
}
