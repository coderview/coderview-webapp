import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ProgrammingLanguageService} from './service/interview/programming-language.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {HeaderComponent} from './header/header.component';
import {InterviewEditComponent} from './interview/interview-edit/interview-edit.component';
import {InterviewEnvironmentComponent} from './interview/interview-environment/interview-environment.component';
import {InterviewsListComponent} from './interview/interviews-list/interviews-list.component';
import {InterviewDetailsComponent} from './interview/interview-details/interview-details.component';
import {InterviewComponent} from './interview/interview.component';
import {LoginComponent} from './auth/login/login.component';
import {RegisterComponent} from './auth/register/register.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from './service/auth/auth.service';
import {AuthGuard} from './service/auth/auth-guard.service';
import {HomeComponent} from './home/home.component';
import {AuthInterceptor} from './service/auth/auth-interceptor.service';
import {DropdownDirective} from './directives/dropdown.directive';
import {ClickOutsideModule} from 'ng-click-outside';
import {SearchService} from './service/search/search.service';
import {SolrInterviewListTransformService} from './service/transform/solr-interview-list-transform.service';
import {SearchParamsBuilder} from './util/search-params.builder';
import {InterviewService} from './service/interview/interview.service';
import {InterviewResolveService} from './service/interview/interview-resolve.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {UserService} from './service/user/user.service';
import {ClipboardModule} from 'ngx-clipboard';
import {TabComponentComponent} from './interview/interview-environment/tabs-toolbar/tab-component/tab-component.component';
import {TabService} from './service/interview/tab.service';
import {SafePipe} from './pipes/safe.pipe';
import {InterviewSetupComponent} from './interview/interview-setup/interview-setup.component';
import {AngularSplitModule} from 'angular-split';
import {SetupModeAlertComponent} from './interview/interview-environment/setup-mode-alert/setup-mode-alert.component';
import {TabsToolbarComponent} from './interview/interview-environment/tabs-toolbar/tabs-toolbar.component';
import {NewTabDropdownComponent} from './interview/interview-environment/tabs-toolbar/new-tab-dropdown/new-tab-dropdown.component';
import {NoActiveTabComponent} from './interview/interview-environment/no-active-tab/no-active-tab.component';
import {OutputComponent} from './interview/interview-environment/output/output.component';
import {NotesComponent} from './interview/interview-environment/notes/notes.component';
import {InterviewManagementToolbarComponent} from './interview/interview-environment/interview-management-toolbar/interview-management-toolbar.component';
import {InterviewSessionComponent} from './interview/interview-session/interview-session.component';
import { ChallengesComponent } from './challenges/challenges.component';
import { ChallengeEditComponent } from './challenges/challenge-edit/challenge-edit.component';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {MonacoEditorModule} from 'ngx-monaco-editor';
import { ChallengeDetailsComponent } from './challenges/challenge-details/challenge-details.component';
import { InterviewEndComponent } from './interview/interview-end/interview-end.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    InterviewEditComponent,
    InterviewEnvironmentComponent,
    InterviewsListComponent,
    InterviewDetailsComponent,
    InterviewComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    DropdownDirective,
    TabComponentComponent,
    SafePipe,
    InterviewSetupComponent,
    SetupModeAlertComponent,
    TabsToolbarComponent,
    NewTabDropdownComponent,
    NoActiveTabComponent,
    OutputComponent,
    NotesComponent,
    InterviewManagementToolbarComponent,
    InterviewSessionComponent,
    ChallengesComponent,
    ChallengeEditComponent,
    ChallengeDetailsComponent,
    InterviewEndComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    ClickOutsideModule,
    NgbModule,
    ClipboardModule,
    AngularSplitModule.forRoot(),
    CKEditorModule,
    MonacoEditorModule.forRoot()
  ],
  providers: [
    ProgrammingLanguageService,
    AuthService,
    AuthGuard,
    SearchService,
    SolrInterviewListTransformService,
    SearchParamsBuilder,
    InterviewService,
    InterviewResolveService,
    UserService,
    TabService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
