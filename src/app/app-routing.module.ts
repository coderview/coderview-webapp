import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {InterviewsListComponent} from './interview/interviews-list/interviews-list.component';
import {InterviewEditComponent} from './interview/interview-edit/interview-edit.component';
import {InterviewDetailsComponent} from './interview/interview-details/interview-details.component';
import {InterviewEnvironmentComponent} from './interview/interview-environment/interview-environment.component';
import {LoginComponent} from './auth/login/login.component';
import {RegisterComponent} from './auth/register/register.component';
import {AuthGuard} from './service/auth/auth-guard.service';
import {HomeComponent} from './home/home.component';
import {InterviewResolveService} from './service/interview/interview-resolve.service';
import {InterviewSetupComponent} from './interview/interview-setup/interview-setup.component';
import {InterviewSessionComponent} from './interview/interview-session/interview-session.component';
import {InterviewJoinResolveService} from './service/interview/interview-join-resolve.service';
import {ChallengesComponent} from './challenges/challenges.component';
import {ChallengeEditComponent} from './challenges/challenge-edit/challenge-edit.component';
import {ChallengeResolveService} from './service/challenge/challenge-resolve.service';
import {InterviewEndComponent} from './interview/interview-end/interview-end.component';


const routes: Routes = [
  {path: '', component: HomeComponent, pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'i/:link', component: InterviewSessionComponent, resolve: {interview: InterviewJoinResolveService}},
  {path: 'i/:link/end', component: InterviewEndComponent},
  {path: 'interviews', component: InterviewsListComponent, canActivate: [AuthGuard]},
  {path: 'interviews/new', component: InterviewEditComponent, canActivate: [AuthGuard]},
  {path: 'interviews/:id', component: InterviewDetailsComponent, canActivate: [AuthGuard], resolve: {interview: InterviewResolveService}},
  {path: 'interviews/:id/setup', component: InterviewSetupComponent, canActivate: [AuthGuard], resolve: {interview: InterviewResolveService}},
  {path: 'interviews/:id/edit', component: InterviewEditComponent, canActivate: [AuthGuard], resolve: {interview: InterviewResolveService}},
  {path: 'interviews/:id/environment', component: InterviewEnvironmentComponent, canActivate: [AuthGuard]},
  {path: 'challenges', component: ChallengesComponent, canActivate: [AuthGuard]},
  {path: 'challenges/new/private', component: ChallengeEditComponent, data: {scope: 'private'}, canActivate: [AuthGuard]},
  {path: 'challenges/new/global', component: ChallengeEditComponent, data: {scope: 'global'}, canActivate: [AuthGuard]},
  {path: 'challenges/:id/edit', component: ChallengeEditComponent, resolve: {challenge: ChallengeResolveService}}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
