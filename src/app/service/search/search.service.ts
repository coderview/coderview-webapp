import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {SolrInterviewListTransformService} from '../transform/solr-interview-list-transform.service';
import {map} from 'rxjs/operators';
import {SearchCriteria} from '../../model/search/search-criteria.model';
import {SearchParamsBuilder} from '../../util/search-params.builder';
import {InterviewDocumentList} from '../../model/search/interview-document-list.model';
import {ChallengeDocumentList} from '../../model/search/challenge-document-list.model';
import {Observable} from 'rxjs';

@Injectable()
export class SearchService {

  constructor(private http: HttpClient,
              private solrInterviewTransformService: SolrInterviewListTransformService,
              private searchParamsBuilder: SearchParamsBuilder) {
  }

  getInterviews(searchCriteria: SearchCriteria) {
    return this.http.get<InterviewDocumentList>(environment.searchApiBaseUri + '/search/interviews', {
      params: this.searchParamsBuilder.build(searchCriteria)
    })
      .pipe(map(response => this.solrInterviewTransformService.transform(response)));
  }

  getChallenges(searchCriteria: SearchCriteria, scope: string): Observable<ChallengeDocumentList> {
    return this.http.get<ChallengeDocumentList>(environment.searchApiBaseUri + '/search/challenges/' + scope, {
      params: this.searchParamsBuilder.build(searchCriteria)
    });
  }
}
