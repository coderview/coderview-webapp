import {Injectable, OnDestroy} from '@angular/core';
import {SocketClientState} from './socket-client-state.enum';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {Client} from 'stompjs';
import * as SockJS from 'sockjs-client';
import {filter, first, switchMap} from 'rxjs/operators';
import {Message, Stomp, StompSubscription} from '@stomp/stompjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketClientService implements OnDestroy {

  connected = new Subject<boolean>();

  private client;
  private state: BehaviorSubject<SocketClientState>;

  static jsonHandler(message: Message): any {
    return JSON.parse(message.body);
  }

  static textHandler(message: Message): string {
    return message.body;
  }

  constructor() {
    this.state = new BehaviorSubject<SocketClientState>(SocketClientState.DISCONNECTED);
  }

  connect(headers?) {
    if (this.state.getValue() !== SocketClientState.ATTEMPTING && this.state.getValue() !== SocketClientState.CONNECTED) {
      this.client = Stomp.over(new SockJS(environment.socketUrl));
      this.state.next(SocketClientState.ATTEMPTING);
      this.client.connect(headers, () => {
        this.state.next(SocketClientState.CONNECTED);
        this.connected.next(true);
      });
    }
  }

  onMessage(topic: string, handler = SocketClientService.jsonHandler): Observable<any> {
    return this.getClient().pipe(first(), switchMap(client => {
      return new Observable<any>(observer => {
        const subscription: StompSubscription = client.subscribe(topic, (message: Message) => {
          observer.next(handler(message));
        });
        return () => client.unsubscribe(subscription.id);
      });
    }));
  }

  onPlainMessage(topic: string): Observable<string> {
    return this.onMessage(topic, SocketClientService.textHandler);
  }

  send(topic: string, payload: any): void {
    this.getClient()
      .pipe(first())
      .subscribe(client => client.send(topic, {}, JSON.stringify(payload)));
  }

  disconnect(headers?) {
    this.getClient().pipe(first()).subscribe(client => {
      client.disconnect(null, headers);
      this.state.next(SocketClientState.DISCONNECTED);
    });
  }

  getState(): BehaviorSubject<SocketClientState> {
    return this.state;
  }

  private getClient(): Observable<Client> {
    return new Observable<Client>(observer => {
      this.state.pipe(filter(state => state === SocketClientState.CONNECTED)).subscribe(() => {
        observer.next(this.client);
      });
    });
  }

  ngOnDestroy(): void {
    this.getClient().pipe(first()).subscribe(client => client.disconnect(null));
  }
}
