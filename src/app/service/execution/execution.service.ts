import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ExecutionResults} from '../../model/execution/execution-results.model';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExecutionService {

  newExecutionResult = new Subject<ExecutionResults>();

  constructor(private http: HttpClient) {}

  executeTab(tabId: string): Observable<ExecutionResults> {
    return this.http.get<ExecutionResults>(environment.coderviewApiBaseUri + '/execute/tab/' + tabId);
  }
}
