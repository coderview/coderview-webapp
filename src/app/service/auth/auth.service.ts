import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {AuthenticationResponse} from '../../model/security/authentication.model';
import {map} from 'rxjs/operators';
import * as moment from 'moment';
import * as jwt_decode from 'jwt-decode';

@Injectable()
export class AuthService {

  private idToken = 'id_token';
  private expiresAt = 'expires_at';

  constructor(private router: Router, private http: HttpClient) {
  }

  login(email: string, password: string) {
    return this.http.post<AuthenticationResponse>(environment.coderviewApiBaseUri + '/authenticate', {email, password},
      {
        headers: new HttpHeaders().set('Content-Type', 'application/json')
      }).pipe(map(response => this.setSession(response)));
  }

  private setSession(authResponse: AuthenticationResponse) {
    console.log('Inside setSession, authResponse: ' + authResponse);
    const expiresAt = moment(authResponse.expiresAt);

    localStorage.setItem(this.idToken, authResponse.idToken);
    localStorage.setItem(this.expiresAt, JSON.stringify(expiresAt.valueOf()));
  }

  logout() {
    localStorage.removeItem(this.idToken);
    localStorage.removeItem(this.expiresAt);
  }

  getIdToken() {
    return localStorage.getItem(this.idToken);
  }

  isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  getLoggedInUser() {
    const token = this.getIdToken();
    if (token) {
      const decoded = jwt_decode(token);
      return decoded.sub;
    } else {
      return null;
    }
  }

  isAdmin(): boolean {
    const token = this.getIdToken();
    if (token) {
      const decoded = jwt_decode(token);
      return decoded.roles.some(role => role === 'ADMIN');
    } else {
      return false;
    }
  }

  private getExpiration() {
    const expiration = localStorage.getItem(this.expiresAt);
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }
}
