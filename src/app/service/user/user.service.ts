import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../../model/user/user.model';
import {environment} from '../../../environments/environment';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  register(user: User) {
    return this.http.post(environment.coderviewApiBaseUri + '/users/register', user);
  }
}
