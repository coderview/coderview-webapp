import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Challenge} from '../../model/challenge/challenge.model';
import {Tab} from '../../model/interview/tab.model';

@Injectable({
  providedIn: 'root'
})
export class InterviewEnvironmentService {

  constructor(private http: HttpClient) {
  }

  getNotes(environmentId: number): Observable<string> {
    return this.http.get<{ notes: string }>(environment.coderviewApiBaseUri + '/interview/environment/' + environmentId + '/notes')
      .pipe(map(response => response.notes));
  }

  saveNotes(environmentId: number, content: string) {
    return this.http.post(environment.coderviewApiBaseUri + '/interview/environment/' + environmentId + '/notes', {notes: content});
  }

  includeChallenge(environmentId: number, challenge: Challenge): Observable<Tab> {
    return this.http.post<Tab>(environment.coderviewApiBaseUri + '/interview/environment/' + environmentId + '/challenges', challenge);
  }
}
