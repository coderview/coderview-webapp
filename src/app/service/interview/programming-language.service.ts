import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ProgrammingLanguage} from '../../model/interview/programming-language.model';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class ProgrammingLanguageService {

  constructor(private httpClient: HttpClient) {
  }

  getProgrammingLanguages(): Observable<ProgrammingLanguage[]> {
    return this.httpClient.get<ProgrammingLanguage[]>(environment.coderviewApiBaseUri + '/programminglanguages');
  }
}
