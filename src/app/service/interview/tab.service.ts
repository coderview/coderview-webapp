import {Observable, Subject} from 'rxjs';
import {Tab} from '../../model/interview/tab.model';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {share} from 'rxjs/operators';

@Injectable()
export class TabService {
  tabOpen = new Subject<Tab>();
  tabClose = new Subject<Tab>();
  includeChallenge = new Subject<Tab>();

  constructor(private http: HttpClient) {
  }

  createTab(environmentId: number, name: string, programmingLanguage: string): Observable<Tab> {
    return this.http.post<Tab>(environment.coderviewApiBaseUri + '/interview/environment/' + environmentId + '/tabs', {
      title: name,
      programmingLanguage: {
        name: programmingLanguage
      }
    });
  }

  deleteTab(environmentId: number, tabId: string) {
    return this.http.delete(environment.coderviewApiBaseUri + '/interview/environment/' + environmentId + '/tabs/' + tabId);
  }
}
