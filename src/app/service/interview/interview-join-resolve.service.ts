import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Interview} from '../../model/interview/interview.model';
import {InterviewService} from './interview.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InterviewJoinResolveService implements Resolve<Interview> {

  constructor(private interviewService: InterviewService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Interview> | Promise<Interview> | Interview {
    return this.interviewService.join(route.params.link);
  }
}
