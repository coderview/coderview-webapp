import {Injectable} from '@angular/core';
import {InterviewService} from './interview.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Interview} from '../../model/interview/interview.model';
import {Observable} from 'rxjs';

@Injectable()
export class InterviewResolveService implements Resolve<Interview> {

  constructor(private interviewService: InterviewService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Interview> | Promise<Interview> | Interview {
    return this.interviewService.getInterview(+route.params.id);
  }
}
