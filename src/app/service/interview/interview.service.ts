import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Interview} from '../../model/interview/interview.model';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {AuthService} from '../auth/auth.service';
import {Participant} from '../../model/interview/participant.model';

@Injectable()
export class InterviewService {

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  getInterview(id: number): Observable<Interview> {
    return this.http.get<Interview>(environment.coderviewApiBaseUri + '/interviews/' + id)
      .pipe(map(interview => {
        interview.scheduledFor = new Date(interview.scheduledFor);
        return interview;
      }));
  }

  updateInterview(id: number, interview: Interview) {
    return this.http.put(environment.coderviewApiBaseUri + '/interviews/' + id, interview);
  }

  createInterview(interview: Interview) {
    return this.http.post(environment.coderviewApiBaseUri + '/interviews/', interview);
  }

  deleteInterview(id: number) {
    return this.http.delete(environment.coderviewApiBaseUri + '/interviews/' + id);
  }

  start(id: number) {
    return this.http.get(environment.coderviewApiBaseUri + '/interviews/' + id + '/start');
  }

  join(link: string): Observable<Interview> {
    return this.http.get<Interview>(environment.coderviewApiBaseUri + '/interviews/' + link + '/join');
  }

  end(id: number) {
    return this.http.post(environment.coderviewApiBaseUri + '/interviews/' + id + '/end', {});
  }

  isInterviewer(interview: Interview) {
    return this.authService.getLoggedInUser() === interview.interviewer;
  }

  getParticipants(id: number): Observable<Participant[]> {
    return this.http.get<Participant[]>(environment.coderviewApiBaseUri + '/interviews/' + id + '/participants');
  }
}
