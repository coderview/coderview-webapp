import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ChallengeType} from '../../model/challenge/challenge-type.model';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChallengeTypeService {

  constructor(private httpClient: HttpClient) {
  }

  getChallengeTypes(): Observable<ChallengeType[]> {
    return this.httpClient.get<ChallengeType[]>(environment.coderviewApiBaseUri + '/challenges/types');
  }
}
