import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Challenge} from '../../model/challenge/challenge.model';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChallengeService {

  constructor(private httpClient: HttpClient) {
  }

  getChallenge(id: number): Observable<Challenge> {
    return this.httpClient.get<Challenge>(environment.coderviewApiBaseUri + '/challenges/' + id);
  }

  createChallenge(challenge: Challenge): Observable<Challenge> {
    return this.httpClient.post<Challenge>(environment.coderviewApiBaseUri + '/challenges', challenge);
  }

  updateChallenge(id: number, challenge: Challenge): Observable<Challenge> {
    return this.httpClient.put<Challenge>(environment.coderviewApiBaseUri + '/challenges/' + id, challenge);
  }
}
