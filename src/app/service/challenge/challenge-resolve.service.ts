import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Challenge} from '../../model/challenge/challenge.model';
import {Observable} from 'rxjs';
import {ChallengeService} from './challenge.service';

@Injectable({
  providedIn: 'root'
})
export class ChallengeResolveService  implements Resolve<Challenge> {

  constructor(private challengeService: ChallengeService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Challenge> | Promise<Challenge> | Challenge {
    return this.challengeService.getChallenge(+route.params.id);
  }
}
