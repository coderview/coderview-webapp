import {InterviewsListItem} from '../../model/interview/interviews-list-item.model';
import {InterviewDocument} from '../../model/search/interview-document.model';
import {Candidate} from '../../model/interview/candidate.model';
import {InterviewDocumentList} from '../../model/search/interview-document-list.model';
import {InterviewsList} from '../../model/interview/interviews-list.model';

export class SolrInterviewListTransformService {

  transform(interviewDocumentList: InterviewDocumentList): InterviewsList {
    return new InterviewsList(
      +interviewDocumentList.numFound,
      interviewDocumentList.entries.map(doc => this.transformDocument(doc))
    );
  }

  private transformDocument(interviewDocument: InterviewDocument): InterviewsListItem {
    return new InterviewsListItem(
      +interviewDocument.id,
      interviewDocument.title,
      new Candidate(
        interviewDocument.candidateFirstName,
        interviewDocument.candidateLastName,
        interviewDocument.candidateEmail
      ),
      interviewDocument.status,
      new Date(interviewDocument.scheduledFor),
      interviewDocument.link
    );
  }
}
