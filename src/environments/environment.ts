// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  coderviewApiBaseUri: 'http://localhost:8190/coderview-api',
  searchApiBaseUri: 'http://localhost:8192/search-api',
  webappBaseUri: 'http://localhost:4200',
  socketUrl: 'http://localhost:8190/coderview-api/ws',
  etherpadUrl: 'http://127.0.0.1:9001'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
