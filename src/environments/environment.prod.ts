export const environment = {
  production: true,
  coderviewApiBaseUri: 'http://localhost:8190/coderview-api',
  searchApiBaseUri: 'http://localhost:8192/search-api',
  webappBaseUri: 'http://localhost:4200',
  etherpadUrl: 'http://127.0.0.1:9001/p'
};
